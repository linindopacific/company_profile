<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('layout');
	}

	public function index() {
		redirect('about_us/who_we_are', 'location');
	}

	public function who_we_are() {
		$content = $this->load->view('about_us/v_who_we_are', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}

	public function values() {
		$content = $this->load->view('about_us/v_values', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}

	public function vision_mission() {
		$content = $this->load->view('about_us/v_vision_mission', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}
}
