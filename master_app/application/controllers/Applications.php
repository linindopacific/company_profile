<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applications extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('layout');
	}

	public function index() {
		redirect('applications/mining', 'location');
	}

	public function mining() {
		$content = $this->load->view('applications/v_mining', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}

	public function off_highway() {
		$content = $this->load->view('applications/v_off_highway', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}

	public function sea_airports() {
		$content = $this->load->view('applications/v_sea_airports', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}
}
