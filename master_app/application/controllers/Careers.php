<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('layout');
	}

	public function index() {
		$this->load->database();
		$vacancies = $this->db->select('posisi, deskripsi, requirement')
			->where('status', '1')
			->where('published', TRUE)
			->order_by('dateinput', 'desc')
			->get('tr_job_vacancy')
			->result_array();
		$content = $this->load->view('careers/v_index', array(
			'vacancies' => $vacancies
		), TRUE);
		$this->layout->add_content($content);
		$this->layout->render('page');
	}
}
