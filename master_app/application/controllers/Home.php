<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('layout');
	}

	public function index() {
		$content = $this->load->view('home/v_index', NULL, TRUE);
		$this->layout->add_content($content);
		$this->layout->render('home');
	}
}
