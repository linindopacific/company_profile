<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('layout');
	}

	public function index() {
		$products = [
			array(
				'id' => 'work_lamps',
				'name' => 'Work Lamps',
				'image' => base_url() . 'img/products/work_lamps.jpg',
				'images' => [
					base_url() . 'img/products/work_lamps/image1.jpg',
					base_url() . 'img/products/work_lamps/image2.jpg',
					base_url() . 'img/products/work_lamps/image3.jpg',
					base_url() . 'img/products/work_lamps/image4.jpg',
					base_url() . 'img/products/work_lamps/image5.jpg',
					base_url() . 'img/products/work_lamps/image6.jpg',
					base_url() . 'img/products/work_lamps/image7.jpg',
					base_url() . 'img/products/work_lamps/image8.jpg'
				]
			),
			array(
				'id' => 'signal_lamps',
				'name' => 'Signal Lamps',
				'image' => base_url() . 'img/products/signal_lamps.jpg',
				'images' => [
					base_url() . 'img/products/signal_lamps/image1.jpg',
					base_url() . 'img/products/signal_lamps/image2.jpg',
					base_url() . 'img/products/signal_lamps/image3.jpg',
					base_url() . 'img/products/signal_lamps/image4.jpg',
					base_url() . 'img/products/signal_lamps/image5.jpg',
					base_url() . 'img/products/signal_lamps/image6.jpg',
					base_url() . 'img/products/signal_lamps/image7.jpg',
					base_url() . 'img/products/signal_lamps/image8.jpg'
				]
			),
			array(
				'id' => 'warning_lamps',
				'name' => 'Warning Lamps',
				'image' => base_url() . 'img/products/warning_lamps.jpg',
				'images' => [
					base_url() . 'img/products/warning_lamps/image1.jpg',
					base_url() . 'img/products/warning_lamps/image2.jpg',
					base_url() . 'img/products/warning_lamps/image3.jpg',
					base_url() . 'img/products/warning_lamps/image4.jpg',
					base_url() . 'img/products/warning_lamps/image5.jpg',
					base_url() . 'img/products/warning_lamps/image6.jpg',
					base_url() . 'img/products/warning_lamps/image7.jpg',
					base_url() . 'img/products/warning_lamps/image8.jpg'
				]
			),
			array(
				'id' => 'auto_electrical_consumable',
				'name' => 'Auto Electrical Consumable',
				'image' => base_url() . 'img/products/auto_electrical_consumable.jpg',
				'images' => [
					base_url() . 'img/products/auto_electrical_consumable/image1.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image2.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image3.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image4.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image5.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image6.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image7.jpg',
					base_url() . 'img/products/auto_electrical_consumable/image8.jpg'
				]
			),
			array(
				'id' => 'fire_suppression',
				'name' => 'Fire Suppression',
				'image' => base_url() . 'img/products/fire_suppression.jpg',
				'images' => [
					base_url() . 'img/products/fire_suppression/image1.jpg',
					base_url() . 'img/products/fire_suppression/image2.jpg',
					base_url() . 'img/products/fire_suppression/image3.jpg',
					base_url() . 'img/products/fire_suppression/image4.jpg'
				]
			),
			array(
				'id' => 'rotating_parts',
				'name' => 'Rotating Parts',
				'image' => base_url() . 'img/products/rotating_parts.jpg',
				'images' => [
					base_url() . 'img/products/rotating_parts/image1.jpg',
					base_url() . 'img/products/rotating_parts/image2.jpg',
					base_url() . 'img/products/rotating_parts/image3.jpg',
					base_url() . 'img/products/rotating_parts/image4.jpg',
					base_url() . 'img/products/rotating_parts/image5.jpg',
					base_url() . 'img/products/rotating_parts/image6.jpg',
					base_url() . 'img/products/rotating_parts/image7.jpg',
					base_url() . 'img/products/rotating_parts/image8.jpg',
					base_url() . 'img/products/rotating_parts/image9.jpg',
					base_url() . 'img/products/rotating_parts/image10.jpg',
					base_url() . 'img/products/rotating_parts/image11.jpg',
					base_url() . 'img/products/rotating_parts/image12.jpg'
				]
			),
			array(
				'id' => 'battery_testing_equipment',
				'name' => 'Battery &amp; Testing Equipment',
				'image' => base_url() . 'img/products/battery_testing_equipment.jpg',
				'images' => [
					base_url() . 'img/products/battery_testing_equipment/image1.jpg',
					base_url() . 'img/products/battery_testing_equipment/image2.jpg',
					base_url() . 'img/products/battery_testing_equipment/image3.jpg',
					base_url() . 'img/products/battery_testing_equipment/image4.jpg',
					base_url() . 'img/products/battery_testing_equipment/image5.jpg',
					base_url() . 'img/products/battery_testing_equipment/image6.jpg',
					base_url() . 'img/products/battery_testing_equipment/image7.jpg',
					base_url() . 'img/products/battery_testing_equipment/image8.jpg'
				]
			),
			array(
				'id' => 'thermal_management_system',
				'name' => 'Thermal Management System',
				'image' => base_url() . 'img/products/thermal_management_system.jpg',
				'images' => [
					base_url() . 'img/products/thermal_management_system/image1.jpg',
					base_url() . 'img/products/thermal_management_system/image2.jpg',
					base_url() . 'img/products/thermal_management_system/image3.jpg',
					base_url() . 'img/products/thermal_management_system/image4.jpg'
				]
			),
			array(
				'id' => 'filtration_system',
				'name' => 'Filtration System',
				'image' => base_url() . 'img/products/filtration_system.jpg',
				'images' => [
					base_url() . 'img/products/filtration_system/image1.jpg',
					base_url() . 'img/products/filtration_system/image2.jpg',
					base_url() . 'img/products/filtration_system/image3.jpg',
					base_url() . 'img/products/filtration_system/image4.jpg'
				]
			),
			array(
				'id' => 'customized_automation_system',
				'name' => 'Customized Automation System',
				'image' => base_url() . 'img/products/customized_automation_system.jpg',
				'images' => [
					base_url() . 'img/products/customized_automation_system/image1.jpg',
					base_url() . 'img/products/customized_automation_system/image2.jpg',
					base_url() . 'img/products/customized_automation_system/image3.jpg',
					base_url() . 'img/products/customized_automation_system/image4.jpg'
				]
			),
			array(
				'id' => 'pin_bushing',
				'name' => 'Pin &amp; Bushing',
				'image' => base_url() . 'img/products/pin_bushing.jpg',
				'images' => [
					base_url() . 'img/products/pin_bushing/image1.jpg',
					base_url() . 'img/products/pin_bushing/image2.jpg',
					base_url() . 'img/products/pin_bushing/image3.jpg',
					base_url() . 'img/products/pin_bushing/image4.jpg',
					base_url() . 'img/products/pin_bushing/image5.jpg',
					base_url() . 'img/products/pin_bushing/image6.jpg',
					base_url() . 'img/products/pin_bushing/image7.jpg',
					base_url() . 'img/products/pin_bushing/image8.jpg'
				]
			),
			array(
				'id' => 'accessories',
				'name' => 'Accessories',
				'image' => base_url() . 'img/products/accessories.jpg',
				'images' => [
					base_url() . 'img/products/accessories/image1.jpg',
					base_url() . 'img/products/accessories/image2.jpg',
					base_url() . 'img/products/accessories/image3.jpg',
					base_url() . 'img/products/accessories/image4.jpg',
					base_url() . 'img/products/accessories/image5.jpg',
					base_url() . 'img/products/accessories/image6.jpg',
					base_url() . 'img/products/accessories/image7.jpg',
					base_url() . 'img/products/accessories/image8.jpg',
					base_url() . 'img/products/accessories/image9.jpg',
					base_url() . 'img/products/accessories/image10.jpg',
					base_url() . 'img/products/accessories/image11.jpg',
					base_url() . 'img/products/accessories/image12.jpg'
				]
			)
		];
		$content = $this->load->view('products/v_index', array(
			'products' => $products
		), TRUE);
		$this->layout->add_content($content);
		$this->layout->add_script($this->load->view('products/vjs_index', NULL, TRUE));
		$this->layout->render('page');
	}
}
