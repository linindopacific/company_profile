<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout {
    protected $ci;
    protected $title;
    protected $meta_description;
    protected $content;
    protected $js;
    protected $css;
    protected $script;
    protected $page_title;
    protected $breadcrumbs = [];

    function __construct() {
        $this->ci =& get_instance();
        $this->title = 'Linindo Pacific International';
        $this->meta_description = 'Linindo Pacific International (LPI) is South East Asia\'s largest integrated after market solutions provider for heavy mobile equipment industry. We provide creative integration of products and services in the world of Vehicle Safety and Automation systems, Heavy-duty Automotive Electrical components, Thermal management';
        $this->content = '';
        $this->js = [
            base_url() . 'js/jquery-1.11.1.min.js',
            base_url() . 'js/owl.carousel.min.js',
            base_url() . 'js/bootstrap.min.js',
            base_url() . 'js/wow.min.js',
            base_url() . 'js/typewriter.js',
            base_url() . 'js/jquery.onepagenav.js',
            base_url() . 'assets/slick/slick.min.js',
            base_url() . 'js/main.js'
        ];
        $this->css = [
            base_url() . 'css/normalize.css',
            base_url() . 'css/bootstrap.css',
            base_url() . 'css/owl.css',
            base_url() . 'css/animate.css',
            base_url() . 'css/cardio.css',
            base_url() . 'fonts/font-awesome-4.1.0/css/font-awesome.min.css',
            base_url() . 'assets/slick/slick.css',
            base_url() . 'assets/slick/slick-theme.css',
            base_url() . 'fonts/eleganticons/et-icons.css',
            'https://fonts.googleapis.com/css?family=Cinzel'
        ];
        $this->script = str_replace('<script>', '', $this->ci->load->view('layout/vjs_layout', NULL, TRUE));
        $this->add_breadcrumbs(base_url(), 'Home');
    }

    public function set_title($value = '') {
        $this->title = $title;
    }

    public function set_page_title($title) {
        $this->page_title = $title;
    }

    public function add_breadcrumbs($url, $label = '') {
        if (is_array($url)) {
            foreach ($url as $key => $value) {
                $this->breadcrumbs[] = array('url' => $value['url'], 'label' => $value['label']);
            }
        } else {
            $this->breadcrumbs[] = array('url' => $url, 'label' => $label);
        }
    }

    public function set_meta_description($meta_description = '') {
        $this->meta_description = $this->meta_description;
    }

    public function add_content($content = '') {
        $this->content .= $content;
    }

    public function add_js($js) {
        $this->js[] = $js;
    }

    public function add_css($css) {
        $this->css[] = $css;
    }

    public function add_script($script = '') {
        $script = str_replace('<script>', '', $script);
        $this->script .= $script;
    }

    public function render($layout = NULL) {
        if ($layout && method_exists($this, $layout)) {
            $this->{$layout}();
        } else {
            show_404();
        }
    }

    private function home() {
        $content = $this->ci->load->view('layout/v_navbar', NULL, TRUE);
        $content .= $this->ci->load->view('layout/v_header', NULL, TRUE);
        $content .= $this->content;
        $content .= $this->ci->load->view('layout/v_footer', NULL, TRUE);

        $this->ci->load->view('layout/v_layout', array(
            'title' => $this->title,
            'meta_description' => $this->meta_description,
            'content' => $content,
            'js' => $this->js,
            'css' => $this->css,
            'script' => $this->minify_js($this->script)
        ));
    }

    private function page() {
        $content = $this->ci->load->view('layout/v_navbar_page', NULL, TRUE);
        $content .= $this->content;
        $content .= $this->ci->load->view('layout/v_footer', NULL, TRUE);

        $this->ci->load->view('layout/v_layout', array(
            'title' => $this->title,
            'meta_description' => $this->meta_description,
            'content' => $content,
            'js' => $this->js,
            'css' => $this->css,
            'script' => $this->minify_js($this->script)
        ));
    }

    private function minify_js($input) {
        if (trim($input) === "") return $input;
        return preg_replace(
            array(
                '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
                '#;+\}#',
                '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
                '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
            ),
            array(
                '$1',
                '$1$2',
                '}',
                '$1$3',
                '$1.$3'
            ),
        $input);
    }
}
