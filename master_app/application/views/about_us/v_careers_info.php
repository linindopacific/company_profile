<section class="careers-info bg-red">
    <div class="container text-center">
        <h2 class="light text-center white">Be part of our amazing team!</h2>
        <br />
        <a class="btn btn-vacancies btn-lg" href="<?= base_url('careers'); ?>">Check Our Vacancies</a>
    </div>
</section>
