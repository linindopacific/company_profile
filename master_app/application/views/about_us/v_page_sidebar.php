<ul class="page-sidebar">
    <li class="page-sidebar-item<?= $this->uri->segment(2) == 'who_we_are' ? ' active' : ''; ?>">
        <a href="<?= base_url('about_us/who_we_are'); ?>" class="page-sidebar-link">
            <div class="text-title">Who We Are</div>
        </a>
    </li>
    <li class="page-sidebar-item<?= $this->uri->segment(2) == 'values' ? ' active' : ''; ?>">
        <a href="<?= base_url('about_us/values'); ?>" class="page-sidebar-link">
            <div class="text-title">Values</div>
        </a>
    </li>
    <li class="page-sidebar-item<?= $this->uri->segment(2) == 'vision_mission' ? ' active' : ''; ?>">
        <a href="<?= base_url('about_us/vision_mission'); ?>" class="page-sidebar-link">
            <div class="text-title">Vision &amp; Mission</div>
        </a>
    </li>
</ul>
<?php if (isset($show_image_values) && $show_image_values) { ?>
<br />
<br />
<br />
<div class="img-values">
    <img src="<?= base_url(); ?>img/values/values.png" class="full-width" />
</div>
<?php } ?>
