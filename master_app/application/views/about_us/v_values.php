<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/history.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 md-none">
                <?= $this->load->view('about_us/v_page_sidebar', array('show_image_values' => TRUE), TRUE); ?>
            </div>
            <div class="col-md-8">
                <div class="page-content">
                    <div class="page-title">
                        <h3 class="heading">VALUES</h3>
                    </div>
                    <ul class="core-values">
                        <li data-slide="1">
                            <h4 class="heading">LEADERSHIP</h4>
                            <p>We have the courage to always take charge and the initiative to always lead our clients, our principals and ourselves in whatever we are doing and resume full responsibilities to the stakeholders for the outcomes.</p>
                        </li>
                        <li data-slide="2">
                            <h4 class="heading">PASSIONATE</h4>
                            <p>We are inspired and believe in what we are doing and we go extra mile to achieve a purpose much larger than ourselves.</p>
                        </li>
                        <li data-slide="3">
                            <h4 class="heading">INTEGRITY</h4>
                            <p>We are being ethically unyielding and honest and inspiring trust by saying what we mean, matching our behaviors to our words and taking responsibility for our actions.</p>
                        </li>
                        <li data-slide="4">
                            <h4 class="heading">CONTINUOUS IMPROVEMENT</h4>
                            <p>We are committed to continuously improving both our companies and employees by attracting, developing and retaining the best talent for our business, challenging our people, demonstrating a "<i>can-do</i>" attitude and fostering a collaborative and mutually supportive environment for constant improvement.</p>
                        </li>
                        <li data-slide="5">
                            <h4 class="heading">EXCELLENCE</h4>
                            <p>We continuously strive to achieve the highest standard of result that goes beyond what is expected.</p>
                        </li>
                        <li data-slide="6">
                            <h4 class="heading">PROACTIVE</h4>
                            <p>We always seek to be the first in our market with our ideas and solutions and we will always seek to be solving problems before they occur.</p>
                        </li>
                        <li data-slide="7">
                            <h4 class="heading">ADDED VALUES</h4>
                            <p>We are being responsive to the dynamism of the industry by constantly delivering maximum added values of our solutions to empower clients to become sustainable and high performance businesses.</p>
                        </li>
                        <li data-slide="8">
                            <h4 class="heading">TEAMWORK</h4>
                            <p>We promote and establish a multicultural workforce based on trust and respect to achieve our goals by interacting appropriately.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->load->view('about_us/v_careers_info', NULL, TRUE); ?>
