<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/history.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 md-none">
                <?= $this->load->view('about_us/v_page_sidebar', NULL, TRUE); ?>
            </div>
            <div class="col-md-8">
                <div class="page-content">
                    <div class="page-title">
                        <h3 class="heading">VISION</h3>
                    </div>
                    <p class="text-justify">To be a globally recognized aftermarket integrated solutions provider for heavy mobile equipment.</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <div class="page-title">
                        <h3 class="heading">MISSION</h3>
                    </div>
                    <h5>To be a business entity that</h5>
                    <ul class="mission">
                        <li><p>is committed to help our customers to grow by utilizing our comprehensive understanding through continuous interaction</p></li>
                        <li><p>provides conducive environment and opportunities for our people to challenge themselves and enhance their selves fulfillment based on their valuable achievement</p></li>
                        <li><p>enables creative integration of our products and knowledge to significantly reduce the unscheduled down time and to increase the productivity in the safest way possible throughout our clients</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->load->view('about_us/v_careers_info', NULL, TRUE); ?>
