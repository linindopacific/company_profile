<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/history.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 md-none">
                <?= $this->load->view('about_us/v_page_sidebar', NULL, TRUE); ?>
            </div>
            <div class="col-md-8">
                <div class="page-content">
                    <div class="page-title">
                        <h3 class="heading">WHO WE ARE</h3>
                    </div>
                    <div style="margin: 20px;" class="pull-right">
                        <img src="<?= base_url(); ?>img/logo_600x600.png" style="width:200px;" />
                    </div>
                    <p class="text-justify">LPI is South East Asia's largest Automotive Electrical, Thermal Management, Fire Suppression and Mechatronics Integrated Solutions for Heavy Mobile Equipment ( HME ). With 10+ branches and 200+ employees all over the region and the philosophy focused on continuous improvement, the business offers customers a service level unparalleled in the HME industry.</p>
                    <p class="text-justify">Established in 1999, LPI is now one of the most recognized and respectable player in the industry for its ability to tailor and to integrate the high quality products with professional engineering services to all of its clients, predominantly in the mining industries in the region.</p>
                    <p class="text-justify">LPI boasts one of the largest network as the foundation for its sophisticated supply chain solutions and this has allowed LPI to reach all of his clients' establishment even in the most remote area in the region. Its position as direct importer combining with such a large portfolio of highly respected clients in the world significantly reduces the risk that the business faces, as evidenced by our strong financial growth through tough economic periods.</p>
                    <p class="text-justify">Having been servicing the market for 20 years, LPI has learnt to adapt to suit the dynamism of the industry. Committed to provide only the best to our clients as well as to creative solutions have been instrumental to our continued success.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->load->view('about_us/v_careers_info', NULL, TRUE); ?>
