<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/applications.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 md-none">
                <?= $this->load->view('applications/v_page_sidebar', NULL, TRUE); ?>
            </div>
            <div class="col-md-8">
                <div class="page-content">
                    <div class="panel panel-default panel-image-gallery">
                        <div class="panel-heading">
                            <ul class="image-thumb">
                                <?php for ($x = 1; $x <= 6; $x++) { ?>
                                <li><img src="<?= base_url() . 'img/applications/mining/image' . $x . '_thumb.jpg'; ?>" /></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <ul class="image-preview">
                                <?php for ($x = 1; $x <= 6; $x++) { ?>
                                <li><img src="<?= base_url() . 'img/applications/mining/image' . $x . '.jpg'; ?>" /></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->load->view('applications/v_careers_info', NULL, TRUE); ?>
