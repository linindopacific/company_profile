<ul class="page-sidebar">
    <li class="page-sidebar-item<?= $this->uri->segment(2) == 'mining' ? ' active' : ''; ?>">
        <img src="<?= base_url(); ?>img/applications/mining.jpg" class="full-width" />
        <a href="<?= base_url('applications/mining'); ?>" class="page-sidebar-mask">
            <div class="text-title">Mining</div>
        </a>
    </li>
    <li class="page-sidebar-item<?= $this->uri->segment(2) == 'off_highway' ? ' active' : ''; ?>">
        <img src="<?= base_url(); ?>img/applications/off_highway.jpg" class="full-width" />
        <a href="<?= base_url('applications/off_highway'); ?>" class="page-sidebar-mask">
            <div class="text-title">Off Highway</div>
        </a>
    </li>
    <li class="page-sidebar-item<?= $this->uri->segment(2) == 'sea_airports' ? ' active' : ''; ?>">
        <img src="<?= base_url(); ?>img/applications/sea_airports.jpg" class="full-width" />
        <a href="<?= base_url('applications/sea_airports'); ?>" class="page-sidebar-mask">
            <div class="text-title">Sea &amp; Airports</div>
        </a>
    </li>
</ul>
