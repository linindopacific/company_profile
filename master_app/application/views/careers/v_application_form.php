<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/our_team.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <h3 class="text-red text-center">Formulir Aplikasi</h3>
        <hr class="divider-line" />
		<?= form_open_multipart('careers/application_form', array('id' => 'saveform')); ?>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-6">
            					<div class="form-group">
            						<label class="text-red">Posisi Yang Dilamar</label>
            						<input type="text" name="posisi_yg_dlmr" class="form-control posisi_yg_dlmr" required>
            					</div>
            				</div>
            				<div class="col-lg-3 col-md-4 col-sm-6">
            					<div class="form-group">
            						<label class="text-red">Sumber Informasi Lowongan</label>
            						<select name="sumber_inf_lwngn" class="form-control sumber_inf_lwngn" required>
            							<option value="">----</option>
            							<option value="Jobstreet">Jobstreet</option>
            							<option value="Jobsdb">Jobsdb</option>
            							<option value="Linkedin">Linkedin</option>
            							<option value="Kampus">Kampus</option>
            							<option value="Teman/Keluarga">Teman/Keluarga</option>
            							<option value="Instagram">Instagram</option>
            							<option value="Facebook">Facebook</option>
            							<option value="Lainnya">Lainnya</option>
            						</select>
            					</div>
            				</div>
            				<div class="col-lg-3 col-md-4 col-sm-6">
            					<div class="form-group">
            						<label class="text-red">Foto</label>
            						<input type="file" name="foto" required>
            					</div>
            				</div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseA" aria-expanded="false"><strong>A. DATA PRIBADI</strong></a>
                    </div>
                    <div id="collapseA" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Nama Lengkap</label>
            							<input type="text" name="nm_lengkap" class="form-control nm_lengkap" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Jenis Kelamin</label>
            							<select name="jns_kelamin" class="form-control jns_kelamin" required>
            								<option value="">----</option>
            								<option value="Laki laki">Laki laki</option>
            								<option value="Perempuan">Perempuan</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Email</label>
            							<input type="email" name="email" class="form-control email" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Agama</label>
            							<select name="agama" class="form-control agama" required>
            								<option value="">----</option>
            								<option value="Islam">Islam</option>
            								<option value="Hindu">Hindu</option>
            								<option value="Budha">Buddha</option>
            								<option value="Kristen">Kristen</option>
            								<option value="Kong Hu Cu">Kong Hu Cu</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Status</label>
            							<select name="status" class="form-control status" required>
            								<option value="">----</option>
            								<option value="Single">Single</option>
            								<option value="Menikah">Menikah</option>
            								<option value="Cerai">Cerai</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Tempat Lahir</label>
            							<input type="text" name="tmpt_lahir" class="form-control tmpt_lahir" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Tanggal Lahir</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control tgl_lahir" name="tgl_lahir" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Tinggi Badan</label>
            							<input type="text" name="tg_badan" class="form-control tg_badan" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Berat Badan</label>
            							<input type="text" name="br_badan" class="form-control br_badan" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Warga Negara</label>
            							<select name="wrg_negara" class="form-control wrg_negara" required>
            								<option value="">----</option>
            								<option value="WNI">WNI</option>
            								<option value="WNA">WNA</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>Suku</label>
            							<input type="text" name="suku" class="form-control suku">
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">No. Handphone 1</label>
            							<input type="text" name="no_hp_1" class="form-control no_hp_1" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>No. Handphone 2</label>
            							<input type="text" name="no_hp_2" class="form-control no_hp_2">
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>No.Telepon Rumah</label>
            							<input type="text" name="no_tlp_rmh" class="form-control no_tlp_rmh">
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label class="text-red">No.KTP</label>
            							<input type="text" name="no_ktp" class="form-control no_ktp" required>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>Jenis SIM</label>
            							<select name="jns_sim" class="form-control jns_sim">
            								<option value="#">----</option>
            								<option value="C">C</option>
            								<option value="A">A</option>
            								<option value="B1">B1</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>No.SIM </label>
            							<input type="text" name="no_sim" class="form-control no_sim">
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>Masa Berlaku SIM</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control ms_berlaku_sim" name="ms_berlaku_sim" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-lg-3 col-md-4 col-sm-6">
            						<div class="form-group">
            							<label>Kacamata</label>
            							<select name="kacamata" class="form-control kacamata">
            								<option value="Tidak">Tidak</option>
            								<option value="Ya">Ya</option>
            							</select>
            						</div>
            					</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
            						<div class="form-group">
            							<label class="text-red">Alamat KTP</label>
            							<textarea name="alamat_ktp" class="form-control alamat_ktp" style="height:150px;" required></textarea>
            						</div>
                                    <div class="form-group">
            							<label>RT KTP</label>
            							<input type="text" name="rt_ktp" class="form-control rt_ktp" >
            						</div>
            	                       <div class="form-group">
            							<label>RW KTP</label>
            							<input type="text" name="rw_ktp" class="form-control rw_ktp">
            						</div>
            	                       <div class="form-group">
            							<label class="text-red">Kota/Kabupaten KTP</label>
            							<input type="text" name="kota_ktp" class="form-control kota_ktp" required>
            						</div>
            	                       <div class="form-group">
            							<label class="text-red">Provinsi KTP</label>
            							<input type="text" name="provinsi_ktp" class="form-control provinsi_ktp" required>
            						</div>
            	                       <div class="form-group">
            							<label>Kode POS KTP</label>
            							<input type="text" name="kodepos_ktp" class="form-control kodepos_ktp">
            						</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="text-red">Alamat Domisili</label>
                                        <textarea name="alamat_dm" class="form-control alamat_dm" style="height:150px;" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>RT Domisili</label>
                                        <input type="text" name="rt_dm" class="form-control rt_dm">
                                    </div>
                                    <div class="form-group">
                                        <label>RW Domisili</label>
                                        <input type="text" name="rw_dm" class="form-control rw_dm">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-red">Kota/Kabupaten Domisili</label>
                                        <input type="text" name="kota_dm" class="form-control kota_dm" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-red">Provinsi Domisili</label>
                                        <input type="text" name="provinsi_dm" class="form-control provinsi_dm" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Kode POS Domisili</label>
                                        <input type="text" name="kodepos_dm" class="form-control kodepos_dm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseB" aria-expanded="false"><strong>B. INFORMASI KELUARGA </strong><small>(Mohon diisi selengkap mungkin)</small></a>
                    </div>
                    <div id="collapseB" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Ayah</h5>
                            <input type="hidden" name="if_hb_ayah" value="Ayah">
                            <div class="row">
                            	<div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="if_nm_ayah" class="form-control if_nm_ayah" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
            							<label>Tanggal Lahir</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ayah" name="if_tgl_lhr_ayah" required>
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-5 col-sm-12">
            						<div class="form-group">
            							<label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ayah" class="form-control if_pkrjn_pt_ayah" required>
            						</div>
            					</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Ibu</h5>
                            <input type="hidden" name="if_hb_ibu" value="Ibu">
                            <div class="row">
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_ibu" class="form-control if_nm_ibu" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ibu" name="if_tgl_lhr_ibu" required>
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-5 col-sm-12">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ibu" class="form-control if_pkrjn_pt_ibu" required>
            						</div>
            					</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Saudara</h5>
                            <div class="row">
            					<div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_ka_ad_sau1" class="form-control if_nm_ka_ad_sau1">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ka_ad_sau1" name="if_tgl_lhr_ka_ad_sau1" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<select name="if_hb_ka_ad_sau1" class="form-control if_hb_ka_ad_sau1">
            								<option value="#">----</option>
            								<option value="Kakak Laki-laki">Kakak Laki-laki</option>
            								<option value="Kakak Perempuan">Kakak Perempuan</option>
            								<option value="Adik Laki-laki">Adik Laki-laki</option>
            								<option value="Adik Perempuan">Adik Perempuan</option>
            								<option value="Saudara Kembar">Saudara Kembar</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ka_ad_sau1" class="form-control jml if_pkrjn_pt_ka_ad_sau1" >
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
            					<div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_ka_ad_sau2" class="form-control if_nm_ka_ad_sau2">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ka_ad_sau2" name="if_tgl_lhr_ka_ad_sau2" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<select name="if_hb_ka_ad_sau2" class="form-control if_hb_ka_ad_sau2">
            								<option value="#">----</option>
            								<option value="Kakak Laki-laki">Kakak Laki-laki</option>
            								<option value="Kakak Perempuan">Kakak Perempuan</option>
            								<option value="Adik Laki-laki">Adik Laki-laki</option>
            								<option value="Adik Perempuan">Adik Perempuan</option>
            								<option value="Saudara Kembar">Saudara Kembar</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ka_ad_sau2" class="form-control jml if_pkrjn_pt_ka_ad_sau2" >
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
            					<div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_ka_ad_sau3" class="form-control if_nm_ka_ad_sau3">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ka_ad_sau3" name="if_tgl_lhr_ka_ad_sau3" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<select name="if_hb_ka_ad_sau3" class="form-control if_hb_ka_ad_sau3">
            								<option value="#">----</option>
            								<option value="Kakak Laki-laki">Kakak Laki-laki</option>
            								<option value="Kakak Perempuan">Kakak Perempuan</option>
            								<option value="Adik Laki-laki">Adik Laki-laki</option>
            								<option value="Adik Perempuan">Adik Perempuan</option>
            								<option value="Saudara Kembar">Saudara Kembar</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ka_ad_sau3" class="form-control jml if_pkrjn_pt_ka_ad_sau3" >
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
            					<div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_ka_ad_sau4" class="form-control if_nm_ka_ad_sau4">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ka_ad_sau4" name="if_tgl_lhr_ka_ad_sau4" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<select name="if_hb_ka_ad_sau4" class="form-control if_hb_ka_ad_sau4">
            								<option value="#">----</option>
            								<option value="Kakak Laki-laki">Kakak Laki-laki</option>
            								<option value="Kakak Perempuan">Kakak Perempuan</option>
            								<option value="Adik Laki-laki">Adik Laki-laki</option>
            								<option value="Adik Perempuan">Adik Perempuan</option>
            								<option value="Saudara Kembar">Saudara Kembar</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ka_ad_sau4" class="form-control jml if_pkrjn_pt_ka_ad_sau4" >
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
            					<div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_ka_ad_sau5" class="form-control if_nm_ka_ad_sau5">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control if_tgl_lhr_ka_ad_sau5" name="if_tgl_lhr_ka_ad_sau5" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<select name="if_hb_ka_ad_sau5" class="form-control if_hb_ka_ad_sau5">
            								<option value="#">----</option>
            								<option value="Kakak Laki-laki">Kakak Laki-laki</option>
            								<option value="Kakak Perempuan">Kakak Perempuan</option>
            								<option value="Adik Laki-laki">Adik Laki-laki</option>
            								<option value="Adik Perempuan">Adik Perempuan</option>
            								<option value="Saudara Kembar">Saudara Kembar</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_ka_ad_sau5" class="form-control jml if_pkrjn_pt_ka_ad_sau5" >
            						</div>
            					</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Suami/Isteri</h5>
                            <div class="row">
                            	<div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_sutri" class="form-control if_nm_sutri" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                    						<input type="text" class="form-control if_tgl_lhr_sutri" name="if_tgl_lhr_sutri" >
                                            <div class="input-group-addon">
                        						<span class="fa fa-th"></span>
                    						</div>
                    					</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
                                        <input type="text" name="jml" class="form-control jml" value="Suami/Istri">
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_sutri" class="form-control if_pkrjn_pt_sutri">
            						</div>
            					</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Anak</h5>
                            <div class="row">
                                <div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_anak1" class="form-control if_nm_anak1" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                    						<input type="text" class="form-control if_tgl_lhr_anak1" name="if_tgl_lhr_anak1" >
                    						<div class="input-group-addon">
                    							<span class="fa fa-th"></span>
                    						</div>
                    					</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<input type="text" name="jml" class="form-control jml" placeholder="Anak1" value="Anak1">
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_anak1" class="form-control if_pkrjn_pt_anak1" >
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_anak2" class="form-control if_nm_anak2" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                    						<input type="text" class="form-control if_tgl_lhr_anak2" name="if_tgl_lhr_anak2" >
                    						<div class="input-group-addon">
                    							<span class="fa fa-th"></span>
                    						</div>
                    					</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<input type="text" name="jml" class="form-control jml" placeholder="Anak2" value="Anak2">
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_anak2" class="form-control if_pkrjn_pt_anak2" >
            						</div>
            					</div>
            				</div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-3 col-sm-8">
            						<div class="form-group">
                                        <label>Nama</label>
            							<input type="text" name="if_nm_anak3" class="form-control if_nm_anak3" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                    						<input type="text" class="form-control if_tgl_lhr_anak3" name="if_tgl_lhr_anak3" >
                    						<div class="input-group-addon">
                    							<span class="fa fa-th"></span>
                    						</div>
                    					</div>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-4">
            						<div class="form-group">
                                        <label>Hubungan</label>
            							<input type="text" name="jml" class="form-control jml" placeholder="Anak3" value="Anak3">
            						</div>
            					</div>
            					<div class="col-md-4 col-sm-8">
            						<div class="form-group">
                                        <label>Pekerjaan (Termasuk Nama Perusahaan)</label>
            							<input type="text" name="if_pkrjn_pt_anak3" class="form-control if_pkrjn_pt_anak3" >
            						</div>
            					</div>
            				</div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseC" aria-expanded="false"><strong>C. PENDIDIKAN </strong><small>(Mohon diisi selengkap mungkin)</small></a>
                    </div>
                    <div id="collapseC" class="panel-collapse collapse">
                        <div class="panel-body form-horizontal">
                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">SMA/SMK Sederajat</h5>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Instansi</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="pk_sma_nm_sklh" class="form-control pk_sma_nm_sklh">
        						</div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Jurusan</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="pk_sma_jr" class="form-control pk_sma_jr">
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Masuk</label>
                                        <div class="col-md-4 col-sm-8">
            								<select name="pk_sma_tm" class="form-control pk_sma_tm">
            									<option value="#">----</option>
            								</select>
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Lulus</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_sma_tk" class="form-control pk_sma_tk">
            									<option value="#">----</option>
            								</select>
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Nilai Akhir</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_sma_ipk" class="form-control pk_sma_ipk">
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">No.Ijazah/SKL/NIM</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_sma_noij" class="form-control pk_sma_noij" >
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Status</label>
        						<div class="col-md-2 col-sm-8">
                                    <select name="pk_sma_status" class="form-control pk_sma_status">
    									<option value="#">----</option>
    									<option value="LULUS">LULUS</option>
    									<option value="TIDAK LULUS">TIDAK LULUS</option>
    									<option value="ONGOING">ONGOING</option>
    								</select>
        						</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Diploma</h5>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Instansi</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="pk_d3_nm_sklh" class="form-control pk_d3_nm_sklh" >
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Fakultas</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_d3_fk" class="form-control pk_d3_fk" >
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jurusan/Progdi</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_d3_jr" class="form-control pk_d3_jr" >
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Masuk</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_d3_tm" class="form-control pk_d3_tm">
                        						<option value="#">----</option>
                        					</select>
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Lulus</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_d3_tk" class="form-control pk_d3_tk">
                        						<option value="#">----</option>
                        					</select>
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Nilai Akhir</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_d3_ipk" class="form-control pk_d3_ipk">
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">No.Ijazah/SKL/NIM</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_d3_noij" class="form-control pk_d3_noij">
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Status</label>
        						<div class="col-md-2 col-sm-8">
                                    <select name="pk_d3_status" class="form-control pk_d3_status">
                						<option value="#">----</option>
                						<option value="LULUS">LULUS</option>
                						<option value="TIDAK LULUS">TIDAK LULUS</option>
                						<option value="ONGOING">ONGOING</option>
                					</select>
        						</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Sarjana(S1)</h5>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Instansi</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="pk_s1_nm_sklh" class="form-control pk_s1_nm_sklh" >
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Fakultas</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_s1_fk" class="form-control pk_s1_fk">
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jurusan/Progdi</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_s1_jr" class="form-control pk_s1_jr">
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Masuk</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_s1_tm" class="form-control pk_s1_tm">
                        						<option value="#">----</option>
                        					</select>
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Lulus</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_s1_tk" class="form-control pk_s1_tk">
                        						<option value="#">----</option>
                        					</select>
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Nilai Akhir</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_s1_ipk" class="form-control pk_s1_ipk">
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">No.Ijazah/SKL/NIM</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_s1_noij" class="form-control pk_s1_noij" >
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Status</label>
        						<div class="col-md-2 col-sm-8">
                                    <select name="pk_s1_status" class="form-control pk_s1_status">
                						<option value="#">----</option>
                						<option value="LULUS">LULUS</option>
                						<option value="TIDAK LULUS">TIDAK LULUS</option>
                						<option value="ONGOING">ONGOING</option>
                					</select>
        						</div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Magister(S2)</h5>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Instansi</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="pk_s2_nm_sklh" class="form-control pk_s2_nm_sklh">
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Fakultas</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_s2_fk" class="form-control pk_s2_fk">
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jurusan/Progdi</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_s2_jr" class="form-control pk_s2_jr">
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Masuk</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_s2_tm" class="form-control pk_s2_tm">
            									<option value="#">----</option>
            								</select>
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Tahun Lulus</label>
                                        <div class="col-md-4 col-sm-8">
                                            <select name="pk_s2_tk" class="form-control pk_s2_tk">
            									<option value="#">----</option>
            								</select>
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Nilai Akhir</label>
                                        <div class="col-md-4 col-sm-8">
            								<input type="text" name="pk_s2_ipk" class="form-control pk_s2_ipk">
            							</div>
            						</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">No.Ijazah/SKL/NIM</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input type="text" name="pk_s2_noij" class="form-control pk_s2_noij">
            							</div>
            						</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Status</label>
        						<div class="col-md-2 col-sm-8">
                                    <select name="pk_s2_status" class="form-control pk_s2_status">
    									<option value="#">----</option>
    									<option value="LULUS">LULUS</option>
    									<option value="TIDAK LULUS">TIDAK LULUS</option>
    									<option value="ONGOING">ONGOING</option>
    								</select>
        						</div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseD" aria-expanded="false"><strong>D. PENDIDIKAN NON FORMAL </strong><small>(Termasuk kursus, training, seminaar, dan lain-lain. Jika tidak ada silahkan diisi dengan simbol minus (-) )</small></a>
                    </div>
                    <div id="collapseD" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Nama Kursus/Training/Seminar</label>
            							<input type="text" name="pnf_nm_kr1" class="form-control pnf_nm_kr1" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Tanggal</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control pnf_tgl1" name="pnf_tgl1" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Penyelenggara</label>
            							<input type="text" name="pnf_pnylnggr1" class="form-control pnf_pnylnggr1">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Bersertifikat</label>
            							<select name="pnf_ctt1" class="form-control pnf_ctt1">
            								<option value="Tidak">Tidak</option>
            								<option value="Ya">Ya</option>
            							</select>
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Nama Kursus/Training/Seminar</label>
            							<input type="text" name="pnf_nm_kr2" class="form-control pnf_nm_kr2" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Tanggal</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control pnf_tgl2" name="pnf_tgl2" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Penyelenggara</label>
            							<input type="text" name="pnf_pnylnggr2" class="form-control pnf_pnylnggr2">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Bersertifikat</label>
            							<select name="pnf_ctt2" class="form-control pnf_ctt2">
            								<option value="Tidak">Tidak</option>
            								<option value="Ya">Ya</option>
            							</select>
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Nama Kursus/Training/Seminar</label>
            							<input type="text" name="pnf_nm_kr3" class="form-control pnf_nm_kr3" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Tanggal</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control pnf_tgl3" name="pnf_tgl3" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Penyelenggara</label>
            							<input type="text" name="pnf_pnylnggr3" class="form-control pnf_pnylnggr3">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Bersertifikat</label>
            							<select name="pnf_ctt3" class="form-control pnf_ctt3">
            								<option value="Tidak">Tidak</option>
            								<option value="Ya">Ya</option>
            							</select>
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Nama Kursus/Training/Seminar</label>
            							<input type="text" name="pnf_nm_kr4" class="form-control pnf_nm_kr4" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Tanggal</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control pnf_tgl4" name="pnf_tgl4" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Penyelenggara</label>
            							<input type="text" name="pnf_pnylnggr4" class="form-control pnf_pnylnggr4">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Bersertifikat</label>
            							<select name="pnf_ctt4" class="form-control pnf_ctt4">
            								<option value="Tidak">Tidak</option>
            								<option value="Ya">Ya</option>
            							</select>
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Nama Kursus/Training/Seminar</label>
            							<input type="text" name="pnf_nm_kr5" class="form-control pnf_nm_kr5" >
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Tanggal</label>
            							<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            								<input type="text" class="form-control pnf_tgl5" name="pnf_tgl5" >
            								<div class="input-group-addon">
            									<span class="fa fa-th"></span>
            								</div>
            							</div>
            						</div>
            					</div>
                                <div class="col-md-4 col-sm-8">
            						<div class="form-group">
            							<label>Penyelenggara</label>
            							<input type="text" name="pnf_pnylnggr5" class="form-control pnf_pnylnggr5">
            						</div>
            					</div>
            					<div class="col-md-2 col-sm-4">
            						<div class="form-group">
            							<label>Bersertifikat</label>
            							<select name="pnf_ctt5" class="form-control pnf_ctt5">
            								<option value="Tidak">Tidak</option>
            								<option value="Ya">Ya</option>
            							</select>
            						</div>
            					</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseE" aria-expanded="false"><strong>E. PENGALAMAN ORGANISASI DAN PRESTASI </strong><small>(Termasuk club, keanggotaan profesional, dan lain-lain. Jika tidak ada silahkan diisi dengan simbol minus (-). Mohon diisi selengkap mungkin )</small></a>
                    </div>
                    <div id="collapseE" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama Organisasi</label>
            							<input type="text" name="pbp_nmorg1" class="form-control pbp_nmorg1">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Periode</label>
            							<input type="text" name="pbp_prd1" class="form-control pbp_prd1">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Posisi</label>
            							<input type="text" name="pbp_pss1" class="form-control pbp_pss1">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Prestasi</label>
            							<input type="text" name="pbp_prsts1" class="form-control pbp_prsts1">
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama Organisasi</label>
            							<input type="text" name="pbp_nmorg2" class="form-control pbp_nmorg2">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Periode</label>
            							<input type="text" name="pbp_prd2" class="form-control pbp_prd2">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Posisi</label>
            							<input type="text" name="pbp_pss2" class="form-control pbp_pss2">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Prestasi</label>
            							<input type="text" name="pbp_prsts2" class="form-control pbp_prsts2">
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama Organisasi</label>
            							<input type="text" name="pbp_nmorg3" class="form-control pbp_nmorg3">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Periode</label>
            							<input type="text" name="pbp_prd3" class="form-control pbp_prd3">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Posisi</label>
            							<input type="text" name="pbp_pss3" class="form-control pbp_pss3">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Prestasi</label>
            							<input type="text" name="pbp_prsts3" class="form-control pbp_prsts3">
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama Organisasi</label>
            							<input type="text" name="pbp_nmorg4" class="form-control pbp_nmorg4">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Periode</label>
            							<input type="text" name="pbp_prd4" class="form-control pbp_prd4">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Posisi</label>
            							<input type="text" name="pbp_pss4" class="form-control pbp_pss4">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Prestasi</label>
            							<input type="text" name="pbp_prsts4" class="form-control pbp_prsts4">
            						</div>
            					</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama Organisasi</label>
            							<input type="text" name="pbp_nmorg5" class="form-control pbp_nmorg5">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Periode</label>
            							<input type="text" name="pbp_prd5" class="form-control pbp_prd5">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Posisi</label>
            							<input type="text" name="pbp_pss5" class="form-control pbp_pss5">
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Prestasi</label>
            							<input type="text" name="pbp_prsts5" class="form-control pbp_prsts5">
            						</div>
            					</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseF" aria-expanded="false"><strong>F. RIWAYAT	PEKERJAAN </strong><small>(Mulai dari yang terakhir. Termasuk pengalamaan magang, freelance &amp; part-time. Jika tidak ada silahkan diisi dengan simbol minus (-). Mohon diisi selengkap mungkin )</small></a>
                    </div>
                    <div id="collapseF" class="panel-collapse collapse">
                        <div class="panel-body form-horizontal">
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Perusahaan</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_np_1" class="form-control rp_np_1">
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">No Telepon Kantor</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_notlp_1" class="form-control rp_notlp_1">
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jabatan Awal</label>
                						<div class="col-md-6 col-sm-8">
            								<input type="text" name="rp_jaw_1" class="form-control rp_jaw_1">
                						</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Bulan-Tahun Masuk</label>
                						<div class="col-md-4 col-sm-4">
                                            <div class="input-group date" data-provide="datepicker" data-date-format="mm/yyyy">
            									<input type="text" class="form-control rp_bl_th_mk_1" name="rp_bl_th_mk_1" >
            									<div class="input-group-addon">
            										<span class="fa fa-th"></span>
            									</div>
            								</div>
                						</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jabatan Akhir</label>
                						<div class="col-md-6 col-sm-8">
            								<input type="text" name="rp_jak_1" class="form-control rp_jak_1">
                						</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Bulan-Tahun Keluar</label>
                						<div class="col-md-4 col-sm-4">
                                            <div class="input-group date" data-provide="datepicker" data-date-format="mm/yyyy">
            									<input type="text" class="form-control rp_bl_th_kl_1" name="rp_bl_th_kl_1" >
            									<div class="input-group-addon">
            										<span class="fa fa-th"></span>
            									</div>
            								</div>
                						</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Gaji Terakhir (THP)</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_gj_1" class="form-control rp_gj_1">
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Tugas &amp; Tanggung Jawab Pekerjaan</label>
        						<div class="col-md-5 col-sm-8">
    								<textarea name="rp_jd_1" class="form-control rp_jd_1"> </textarea>
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Alasan Keluar</label>
        						<div class="col-md-5 col-sm-8">
    								<textarea name="rp_ak_1" class="form-control rp_ak_1"> </textarea>
        						</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Perusahaan</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_np_2" class="form-control rp_np_2">
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">No Telepon Kantor</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_notlp_2" class="form-control rp_notlp_2">
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jabatan Awal</label>
                						<div class="col-md-6 col-sm-8">
            								<input type="text" name="rp_jaw_2" class="form-control rp_jaw_2">
                						</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Bulan-Tahun Masuk</label>
                						<div class="col-md-4 col-sm-4">
                                            <div class="input-group date" data-provide="datepicker" data-date-format="mm/yyyy">
            									<input type="text" class="form-control rp_bl_th_mk_2" name="rp_bl_th_mk_2" >
            									<div class="input-group-addon">
            										<span class="fa fa-th"></span>
            									</div>
            								</div>
                						</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jabatan Akhir</label>
                						<div class="col-md-6 col-sm-8">
            								<input type="text" name="rp_jak_2" class="form-control rp_jak_2">
                						</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Bulan-Tahun Keluar</label>
                						<div class="col-md-4 col-sm-4">
                                            <div class="input-group date" data-provide="datepicker" data-date-format="mm/yyyy">
            									<input type="text" class="form-control rp_bl_th_kl_2" name="rp_bl_th_kl_2" >
            									<div class="input-group-addon">
            										<span class="fa fa-th"></span>
            									</div>
            								</div>
                						</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Gaji Terakhir (THP)</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_gj_2" class="form-control rp_gj_2">
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Tugas &amp; Tanggung Jawab Pekerjaan</label>
        						<div class="col-md-5 col-sm-8">
    								<textarea name="rp_jd_2" class="form-control rp_jd_2"> </textarea>
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Alasan Keluar</label>
        						<div class="col-md-5 col-sm-8">
    								<textarea name="rp_ak_2" class="form-control rp_ak_2"> </textarea>
        						</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Nama Perusahaan</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_np_3" class="form-control rp_np_3">
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">No Telepon Kantor</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_notlp_3" class="form-control rp_notlp_3">
        						</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jabatan Awal</label>
                						<div class="col-md-6 col-sm-8">
            								<input type="text" name="rp_jaw_3" class="form-control rp_jaw_3">
                						</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Bulan-Tahun Masuk</label>
                						<div class="col-md-4 col-sm-4">
                                            <div class="input-group date" data-provide="datepicker" data-date-format="mm/yyyy">
            									<input type="text" class="form-control rp_bl_th_mk_3" name="rp_bl_th_mk_3" >
            									<div class="input-group-addon">
            										<span class="fa fa-th"></span>
            									</div>
            								</div>
                						</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Jabatan Akhir</label>
                						<div class="col-md-6 col-sm-8">
            								<input type="text" name="rp_jak_3" class="form-control rp_jak_3">
                						</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
        								<label class="col-md-4 col-sm-4 control-label" style="text-align: left !important;">Bulan-Tahun Keluar</label>
                						<div class="col-md-4 col-sm-4">
                                            <div class="input-group date" data-provide="datepicker" data-date-format="mm/yyyy">
            									<input type="text" class="form-control rp_bl_th_kl_3" name="rp_bl_th_kl_3" >
            									<div class="input-group-addon">
            										<span class="fa fa-th"></span>
            									</div>
            								</div>
                						</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Gaji Terakhir (THP)</label>
        						<div class="col-md-5 col-sm-8">
    								<input type="text" name="rp_gj_3" class="form-control rp_gj_3">
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Tugas &amp; Tanggung Jawab Pekerjaan</label>
        						<div class="col-md-5 col-sm-8">
    								<textarea name="rp_jd_3" class="form-control rp_jd_3"> </textarea>
        						</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-2 col-sm-4 control-label" style="text-align: left !important;">Alasan Keluar</label>
        						<div class="col-md-5 col-sm-8">
    								<textarea name="rp_ak_3" class="form-control rp_ak_3"> </textarea>
        						</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseG" aria-expanded="false"><strong>G. KOMPENSASI DAN BENEFIT </strong><small>(Wajib diisi)</small></a>
                    </div>
                    <div id="collapseG" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Take Home Pay (THP)</h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Saat Ini</label>
                                        <input type="text" name="kb_gj_saat_ini" class="form-control kb_gj_saat_ini">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Yang Diharapkan</label>
                                        <input type="text" name="kb_gj_dhrp" class="form-control kb_gj_dhrp" required>
                                    </div>
                                </div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Komponen Gaji</h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Saat Ini</label>
                                        <textarea  name="kb_komp_saat_ini" class="form-control kb_komp_saat_ini"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Yang Diharapkan</label>
                                        <textarea  name="kb_komp_dhrp" class="form-control kb_komp_dhrp" required></textarea>
                                    </div>
                                </div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Bonus</h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Saat Ini</label>
                                        <input type="text" name="kb_bns_saat_ini" class="form-control kb_bns_saat_ini">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Yang Diharapkan</label>
                                        <input type="text" name="kb_bns_dhrp" class="form-control kb_bns_dhrp" required>
                                    </div>
                                </div>
                            </div>

                            <h5 class="text-muted" style="border-bottom: 1px solid #bdbdbd;">Lain-lain</h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Saat Ini</label>
                                        <input type="text" name="kb_ll_saat_ini" class="form-control kb_ll_saat_ini">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
        								<label>Yang Diharapkan</label>
                                        <input type="text" name="kb_ll_dhrp" class="form-control kb_ll_dhrp" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseH" aria-expanded="false"><strong>H. KEMAMPUAN BERBAHASA ASING &amp; BAHASA DAERAH</strong></a>
                    </div>
                    <div id="collapseH" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Bahasa</label>
            							<input type="text" name="km_bhs1" class="form-control km_bhs1" value="Inggris" readonly >
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Membaca</label>
            							<select name="km_baca1" class="form-control km_baca1" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Menulis</label>
            							<select name="km_tulis1" class="form-control km_tulis1" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Berbicara</label>
            							<select name="km_bicara1" class="form-control km_bicara1" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Bahasa</label>
            							<input type="text" name="km_bhs2" class="form-control km_bhs2" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Membaca</label>
            							<select name="km_baca2" class="form-control km_baca2" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Menulis</label>
            							<select name="km_tulis2" class="form-control km_tulis2" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Berbicara</label>
            							<select name="km_bicara2" class="form-control km_bicara2" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Bahasa</label>
            							<input type="text" name="km_bhs3" class="form-control km_bhs3" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Membaca</label>
            							<select name="km_baca3" class="form-control km_baca3" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Menulis</label>
            							<select name="km_tulis3" class="form-control km_tulis3" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Berbicara</label>
            							<select name="km_bicara3" class="form-control km_bicara3" required>
            								<option value="">----</option>
            								<option value="Baik Sekali">Baik Sekali</option>
            								<option value="Baik">Baik</option>
            								<option value="Cukup">Cukup</option>
            								<option value="Kurang">Kurang</option>
            								<option value="Kurang Sekali">Kurang Sekali</option>
            							</select>
            						</div>
            					</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseI" aria-expanded="false"><strong>I. REFERENSI KERJA </strong><small>(bukan saudara dan sudah kenal minimal 3 tahun)</small></a>
                    </div>
                    <div id="collapseI" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="rk_nm1" class="form-control rk_nm1" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Hubungan</label>
            							<input type="text" name="rk_hbng1" class="form-control rk_hbng1" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>No.Handphone</label>
            							<input type="text" name="rk_hp1" class="form-control rk_hp1" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Alamat</label>
            							<textarea name="rk_al1" class="form-control rk_al1" required></textarea>
            						</div>
            					</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="rk_nm2" class="form-control rk_nm2" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Hubungan</label>
            							<input type="text" name="rk_hbng2" class="form-control rk_hbng2" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>No.Handphone</label>
            							<input type="text" name="rk_hp2" class="form-control rk_hp2" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Alamat</label>
            							<textarea name="rk_al2" class="form-control rk_al2" required></textarea>
            						</div>
            					</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="rk_nm3" class="form-control rk_nm3" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Hubungan</label>
            							<input type="text" name="rk_hbng3" class="form-control rk_hbng3" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>No.Handphone</label>
            							<input type="text" name="rk_hp3" class="form-control rk_hp3" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Alamat</label>
            							<textarea name="rk_al3" class="form-control rk_al3" required></textarea>
            						</div>
            					</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseJ" aria-expanded="false"><strong>J. ORANG YANG DAPAT DIHUBUNGI SAAT SITUASI DARURAT</strong></a>
                    </div>
                    <div id="collapseJ" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="od_nm1" class="form-control od_nm1" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Hubungan</label>
            							<input type="text" name="od_hb1" class="form-control od_hb1" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>No.Handphone</label>
            							<input type="text" name="od_hp1" class="form-control od_hp1" required>
            						</div>
            					</div>
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Alamat</label>
            							<textarea name="od_al1" class="form-control od_al1" required></textarea>
            						</div>
            					</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="od_nm2" class="form-control od_nm2" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Hubungan</label>
            							<input type="text" name="od_hb2" class="form-control od_hb2" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>No.Handphone</label>
            							<input type="text" name="od_hp2" class="form-control od_hp2" required>
            						</div>
            					</div>
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Alamat</label>
            							<textarea name="od_al2" class="form-control od_al2" required></textarea>
            						</div>
            					</div>
                            </div>

                            <hr style="margin: 0px 0px 10px 0px;" />

                            <div class="row">
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Nama</label>
            							<input type="text" name="od_nm3" class="form-control od_nm3" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Hubungan</label>
            							<input type="text" name="od_hb3" class="form-control od_hb3" required>
            						</div>
            					</div>
            					<div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>No.Handphone</label>
            							<input type="text" name="od_hp3" class="form-control od_hp3" required>
            						</div>
            					</div>
                                <div class="col-md-3 col-sm-6">
            						<div class="form-group">
            							<label>Alamat</label>
            							<textarea name="od_al3" class="form-control od_al3" required></textarea>
            						</div>
            					</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseK" aria-expanded="false"><strong>K. DALAM STRUKTUR ORGANISASI, PADA SIAPA SAJA ANDA MELAPOR DAN BERAPA ORANG YANG ANDA PIMPIN DI PERUSAHAAN TERAKHIR/SEKARANG? </strong><small>(Beserta nama jabatannya)</small></a>
                    </div>
                    <div id="collapseK" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
                                <textarea name="gmbr_pss_anda" class="form-control gmbr_pss_anda" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseL" aria-expanded="false"><strong>L. JELASKAN INOVASI/PENCAPAIAN YANG PERNAH ANDA LAKUKAN SELAMA BEKERJA/BERSEKOLAH/BERORGANISASI</strong></a>
                    </div>
                    <div id="collapseL" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
    							<textarea name="inovasi_krj" class="form-control inovasi_krj" ></textarea>
    						</div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseM" aria-expanded="false"><strong>M. JELASKAN KETRAMPILAN KERJA ANDA </strong><small>(Termasuk mesin, alat, aplikasi software dan kemampuan lain)</small></a>
                    </div>
                    <div id="collapseM" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
    							<textarea name="ktrmpln_krj" class="form-control ktrmpln_krj" ></textarea>
    						</div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseN" aria-expanded="false"><strong>N. HOBI </strong><small>(Termasuk aktifitas di waktu luang)</small></a>
                    </div>
                    <div id="collapseN" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
    							<textarea name="hobi" class="form-control hobi" ></textarea>
    						</div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseO" aria-expanded="false"><strong>O. PERNYATAAN</strong></a>
                    </div>
                    <div id="collapseO" class="panel-collapse collapse">
                        <div class="panel-body form-horizontal">
							<div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">1. Waktu yang Anda butuhkan untuk mengundurkan diri dari perusahaan sekarang ?</label>
								<div class="col-sm-2">
    								<select name="pernyataan1" class="form-control pernyataan1" required>
    									<option value="">----</option>
    									<option value="1 Minggu">1 Minggu</option>
    									<option value="2 Minggu">2 Minggu</option>
    									<option value="3 Minggu">3 Minggu</option>
    									<option value="1 Bulan">1 Bulan</option>
    									<option value="Lebih 1 Bulan">Lebih 1 Bulan</option>
    								</select>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">2. Apakah Anda pernah melamar ke grup perusahaan ini sebelumnya? Bila YA, jelaskan kapan dan posisi apa</label>
								<div class="col-sm-3">
    								<textarea name="pernyataan2" class="form-control pernyataan2" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">3. Apakah pernah terlibat dengan pihak Kepolisian? (berkaitan dengan pelanggaran kriminal/persidangan/pelanggaran perdata)</label>
								<div class="col-sm-3">
    								<textarea name="pernyataan3" class="form-control pernyataan3" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">4. Pernah menderita penyakit berat dan penyakit menular?Bila Ya, jelaskan</label>
								<div class="col-sm-3">
    								<textarea name="pernyataan4" class="form-control pernyataan4" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">5. Pernah rawat inap di Rumah Sakit atau di operasi? Bila Ya, jelaskan</label>
								<div class="col-sm-3">
                                    <textarea name="pernyataan5" class="form-control pernyataan5" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">6. Apa ada saudara/teman yang bekerja di perusahaan ini ? Bila Ya, sebutkan</label>
								<div class="col-sm-3">
                                    <textarea name="pernyataan6" class="form-control pernyataan6" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">7. (Untuk wanita) apakah bersedia untuk tidak hamil di masa 1 tahun pertama bekerja?</label>
								<div class="col-sm-3">
                                    <textarea name="pernyataan7" class="form-control pernyataan7"></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">8. Apakah Anda memiliki kerja sampingan?Dimana dan sebagai apa ?</label>
								<div class="col-sm-3">
                                    <textarea name="pernyataan8" class="form-control pernyataan8" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">9. Apakah Anda memiliki kepemilikan/keterikatan dengan perusahaan lain ? Bila Ya, jelaskan</label>
								<div class="col-sm-3">
                                    <textarea name="pernyataan9" class="form-control pernyataan9" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">9. Apakah Anda memiliki kepemilikan/keterikatan dengan perusahaan lain ? Bila Ya, jelaskan</label>
								<div class="col-sm-3">
                                    <textarea name="pernyataan9" class="form-control pernyataan9" required></textarea>
        						</div>
    						</div>
                            <div class="form-group">
								<label class="col-sm-6 control-label" style="text-align: left !important;">10. Jika Anda diterima, kapan Anda dapat mulai kerja ?</label>
								<div class="col-sm-2">
                                    <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
    									<input type="text" class="form-control pernyataan10" name="pernyataan10" required>
    									<div class="input-group-addon">
    										<span class="fa fa-th"></span>
    									</div>
    								</div>
        						</div>
    						</div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapseP" aria-expanded="false"><strong>P. OTORISASI &amp; PENEGASAN</strong></a>
                    </div>
                    <div id="collapseP" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
    							<label>Dengan konfirmasi pernyataan "YA" dibawah ini, maka saya :</label>
    							<ol type="i">
                                    <li class="text-justify">Mengijinkan pembuktian atas informasi di atas dan pertanyaan bila diperlukan untuk menentukan.</li>
                                    <li class="text-justify">Menegaskan bahwa informasi di atas adalah benar dan tepat sejauh pengetahuan saya serta tidak ada niat untuk menyembunyikan fakta.</li>
                                    <li class="text-justify">Menyadari bahwa pemalsuan dan ketidakjujuran dalam pemberian informasi di atas dapat menjadi faktor pembatalan penerimaan kerja maupun pemutusan hubungan kerja dari pihak perusahaan.</li>
    							</ol>
    						</div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="text-align: left !important;">Pernyataan</label>
                                    <div class="col-sm-2">
                						<input type="text" name="pernyataan" class="form-control pernyataan" value="Ya">
                					</div>
                				</div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="text-align: left !important;">Tanggal Pernyataan</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                							<input type="text" class="form-control tgl_pernyataan" name="tgl_pernyataan" required>
                							<div class="input-group-addon">
                								<span class="fa fa-th"></span>
                							</div>
                						</div>
                					</div>
                				</div>
                            </div>
                            <hr style="margin: 0px 0px 10px 0px;" />
                            <div class="form-group pull-right">
            					<div class="img_load"></div>
            					<button type="submit" class="btn btn-primary btn_save">
            						<i class="fa fa-save"></i> Save
            					</button>
            				</div>
                        </div>
                    </div>
                </div>
                <br />
                <div>*) Jika tidak diisi dikasih tanda -</div>
			</div>
        <?= form_close(); ?>
    </div>
</section>
