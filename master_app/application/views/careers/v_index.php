<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/our_team.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3 class="text-red text-center">Why Join Us?</h3>
                <hr class="divider-line" />
                <p class="text-justify text-sm">As one of the fastest growing solution driven aftermarket integrated solutions provider for heavy mobile equipment in the region, LPI is built upon the strength of its people. We are continually improving our business operations by always encouraging innovation through the engagement of our team members across our group of companies. With a vast network throughout Asia Pacific and the overwhelming growth of the company and the whole industry have driven us to challenge the best candidates over the region to take part to strengthen our organization. At LPI, we provide opportunities for growth, personal development, and job satisfaction to all employees, and in return, every team member takes responsibility for improving the company by contributing to the sustainable yet achievable growth for the benefit of the organization.</p>
                <p class="text-justify text-sm">Through this career development framework, we are in a position to retain highly skilled yet to provide an equal opportunity for every employee to reinforce our workforce and enhance our profile as a high quality employer. At LPI, you are invited to explore and develop your personal and professional potential within one of the most dynamic and diverse companies. We value:</p>
                <ul class="vacancy-value">
                    <li><p>Leadership</p></li>
                    <li><p>Courage</p></li>
                    <li><p>Highest level of Employee Satisfaction</p></li>
                    <li><p>Employee Skills Competency</p></li>
                    <li><p>Occupational Health and Safety</p></li>
                    <li><p>Interpersonal Skills Development</p></li>
                </ul>
                <hr />
                <br />
                <br />
                <h3 class="text-center"><a class="btn bg-red btn-lg btn-apply-now" href="https://recruitment.linindopacific.com" target="_blank"><strong>APPLY NOW</strong></a></h3>
            </div>
            <div class="col-md-5">
                <div class="page-content">
                    <h4 class="text-center light">Available Positions</h4>
                    <div class="panel-group job-positions" id="accordion">
                        <?php
                        foreach ($vacancies as $key => $val) {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key; ?>" aria-expanded="false"><?= $val['posisi']; ?></a>
                            </div>
                            <div id="collapse<?= $key; ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p><strong>Description :</strong></p>
                                    <?= $val['deskripsi']; ?>
                                    <br />
                                    <p><strong>Requirements :</strong></p>
                                    <?= $val['requirement']; ?>
                                    <hr />
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
