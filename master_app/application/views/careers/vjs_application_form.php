<script>
$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3d'
    });

    $("#saveform").on('submit',(function(e){
        e.preventDefault();
        var jk = $(".jns_kelamin").val();
        var st = $(".status").val();
        if(jk=="Perempuan"){
            var py = $(".pernyataan7").val();
            if(py==''){
                $(".pernyataan7").focus();
                return false();
            }else{
                if(st=="Menikah"){
                    var nmst = $(".if_nm_sutri").val();
                    var tlst = $(".if_tgl_lhr_sutri").val();
                    var pkst = $(".if_pkrjn_pt_sutri").val();
                    if(nmst==''){
                        $(".if_nm_sutri").focus();
                        return false();
                    }else if(tlst==''){
                        $(".if_tgl_lhr_sutri").focus();
                        return false();
                    }else if(pkst==''){
                        $(".if_pkrjn_pt_sutri").focus();
                        return false();
                    }else{
                        $.ajax({
                            url			: "http://linindopacific.com/linindo/People_careers/SaveFormulirAplikasi",
                            type		: "POST",
                            data		: new FormData(this),
                            contentType	: false,
                            cache		: false,
                            processData	: false,
                            beforeSend  : function(){
                                $(".btn_save").hide();
                                $('.img_load').html("<img src='../assets/images/bx_loader.gif' />Data is being sent...");
                            },
                            success		: function(data){
                                $('.img_load').html("Data is already saved");
                                window.location =  "http://linindopacific.com/linindo/People_careers/SuccessFormAplikasi";
                            }
                        });
                    }
                }else{
                    $.ajax({
                        url			: "http://linindopacific.com/linindo/People_careers/SaveFormulirAplikasi",
                        type		: "POST",
                        data		: new FormData(this),
                        contentType	: false,
                        cache		: false,
                        processData	: false,
                        beforeSend  : function(){
                            $(".btn_save").hide();
                            $('.img_load').html("<img src='../assets/images/bx_loader.gif' />Data is being sent...");
                        },
                        success		: function(data){
                            $('.img_load').html("Data is already saved");
                            window.location =  "http://linindopacific.com/linindo/People_careers/SuccessFormAplikasi";
                        }
                    });
                }
            }
        }else{
            if(st=="Menikah"){
                var nmst = $(".if_nm_sutri").val();
                var tlst = $(".if_tgl_lhr_sutri").val();
                var pkst = $(".if_pkrjn_pt_sutri").val();
                if(nmst==''){
                    $(".if_nm_sutri").focus();
                    return false();
                }else if(tlst==''){
                    $(".if_tgl_lhr_sutri").focus();
                    return false();
                }else if(pkst==''){
                    $(".if_pkrjn_pt_sutri").focus();
                    return false();
                }else{
                    $.ajax({
                        url			: "http://linindopacific.com/linindo/People_careers/SaveFormulirAplikasi",
                        type		: "POST",
                        data		: new FormData(this),
                        contentType	: false,
                        cache		: false,
                        processData	: false,
                        beforeSend  : function(){
                            $(".btn_save").hide();
                            $('.img_load').html("<img src='../assets/images/bx_loader.gif' />Data is being sent...");
                        },
                        success		: function(data){
                            $('.img_load').html("Data is already saved");
                            window.location =  "http://linindopacific.com/linindo/People_careers/SuccessFormAplikasi";
                        }
                    });
                }
            }else{
                $.ajax({
                    url			: "http://linindopacific.com/linindo/People_careers/SaveFormulirAplikasi",
                    type		: "POST",
                    data		: new FormData(this),
                    contentType	: false,
                    cache		: false,
                    processData	: false,
                    beforeSend  : function(){
                        $(".btn_save").hide();
                        $('.img_load').html("<img src='../assets/images/bx_loader.gif' />Data is being sent...");
                    },
                    success		: function(data){
                        $('.img_load').html("Data is already saved");
                        window.location =  "http://linindopacific.com/linindo/People_careers/SuccessFormAplikasi";
                    }
                });
            }
        }

    }));
});
