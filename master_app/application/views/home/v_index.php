<section class="section-focus">
	<div class="cut cut-top"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="intro-tables">
					<div class="intro-table">
						<h2 class="heading hide-hover">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle-thin fa-stack-2x"></i>
								<i class="fa fa-bolt fa-stack-1x"></i>
							</span>
						</h2>
						<h5 class="heading hide-hover">Automotive Electrical</h5>
						<hr />
						<div class="show-hover text-center">
							Work Lamp | Emergency Lamp | Starter Motor | Alternator | HVAC | Battery (Accu) | Autoelectrical Consumable &amp; Accessories
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="intro-tables">
					<div class="intro-table">
						<h2 class="heading hide-hover">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle-thin fa-stack-2x"></i>
								<i class="fa fa-gears fa-stack-1x"></i>
							</span>
						</h2>
						<h5 class="heading hide-hover">Automotive Mechanical</h5>
						<hr />
						<div class="show-hover text-center">
							Pin &amp; Bushing | Cylinder | Fire Suppression System | Filtration System | Fast Filling System | Auto Lubrication System
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="intro-tables">
					<div class="intro-table">
						<h2 class="heading hide-hover">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle-thin fa-stack-2x"></i>
								<i class="fa fa-code-fork fa-rotate-270 fa-stack-1x"></i>
							</span>
						</h2>
						<h5 class="heading hide-hover">Smart Mining System</h5>
						<hr />
						<div class="show-hover text-center">
							Idle Timer | Fatigue Warning | Incline Shutdown | Overspeed Protection System | Payload Management System | Engine Protection System
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section section-padded" id="brands">
	<div class="text-center brand-logo">
		<img src="<?= base_url() . 'img/logos/candelux_b.png'; ?>" alt="Candelux" />
	</div>
	<div class="client-logo">
	<?php for ($x = 0; $x < 50; $x++) { ?>
		<div class="text-center bg-grey">
			<img data-u="image" src="img/brands/brand<?= $x+1; ?>.jpg" />
		</div>
	<?php } ?>
	</div>
</section>
<section id="comodities" class="section">
	<div class="container">
		<div class="row text-center title">
			<h3 class="white text-uppercase">Key Competencies</h3>
			<br />
			<h4 class="light white">LPI offers absolute commitment to quality in both products and services through its familiarity and superior knowledge to the industry and its applications.</h4>
		</div>
		<div class="row">
			<div class="col-md-4">
				<ul class="list-group key-competencies competency-thumb">
					<li class="list-group-item slide-active" data-slide="1">Supply Chain Solutions</li>
					<li class="list-group-item" data-slide="2">After Sales Service</li>
					<li class="list-group-item" data-slide="3">Tailored Products &amp; Solutions</li>
					<li class="list-group-item" data-slide="4">Heavy Mobile Equipment (HME) Fitment &amp; Retrofitting</li>
					<li class="list-group-item" data-slide="5">Multilevel Skilled Labor Supply</li>
					<li class="list-group-item" data-slide="6">HME Electrical Assessment, Audit &amp; Commisioning Programs</li>
					<li class="list-group-item" data-slide="7">Certified HME Professional Training</li>
				</ul>
			</div>
			<div class="col-md-8">
				<ul class="list-group key-competencies competency-master">
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">Supply Chain Solutions</h4>
								<br />
								<p class="text-sm white text-justify">LPI's distribution network boasting an impressive 15 points and branches throughout Indonesia, Singapore, Mongolia, Papua New Guinea ensures a quick response to clients' needs while establishing the reliable stocks availability at clients' doorsteps. This massive network has significantly reduced our clients' inventory costs and this has been the main infrastructure behind LPI's great success.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency1.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">After Sales Service</h4>
								<br />
								<p class="text-sm white text-justify">Behind every great product there is reliable after sales services. We at LPI live with this mentality and attitude. We sell our products with full warranty coverage and LPI has been certified by all of the principals to carry out warranty replacement process in the country and this protection has been greatly appreciated by our clients.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency2.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">Tailored Products &amp; Solutions</h4>
								<br />
								<p class="text-sm white text-justify">We at LPI believe that no one is alike thus we appreciate the unique our clients have. We don't believe in mass solutions thus we produce creative integrations of our products and services and then customize them to suit each unique client. This business model has ensured our clients they have the products and services they need to run their already established operations rather than having them having to change their operations to suit the common products and common services that are available.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency3.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">Heavy Mobile Equipment (HME) Fitment &amp; Retrofitting</h4>
								<br />
								<p class="text-sm white text-justify">The Combination of multi dimension engineering services and high quality products has enabled LPI to offer the High Quality Fitment and Retrofitting of any kind of HME to suit the clients' needs, environment and operation style. LPI is capable of designing, delivering, testing and commissioning of Auto Electrical, Engine Safety Devices and Fire Suppression System without compromising the OEM's warranty policies.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency4.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">Multilevel Skilled Labor Supply</h4>
								<br />
								<p class="text-sm white text-justify">Comprising one of the most prominent learning center boasted by certified and experienced trainers LPI has abundant stocks of highly skilled labor supply and having been servicing the industry for the last 15 years LPI has the most coveted professional in the region.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency5.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">HME Electrical Assessment, Audit &amp; Commisioning Programs</h4>
								<br />
								<p class="text-sm white text-justify">Having been servicing the industry for 15 years LPI offer the most extensive Electrical Assessment, Audit and Commissioning Programs, usually conducted at workshops and jobsites. LPI have been maintaining thousands of machines and have been greatly reducing the unscheduled electrical down time as the result.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency6.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-7">
								<h4 class="white text-justify">Certified HME Professional Training</h4>
								<br />
								<p class="text-sm white text-justify">LPI is one of the most renowned name for the Professional Training Centre and this department has contributed significantly to the group's businesses as well as has been helping people throughout the region to upgrade their skills thoroughly. LPI conduct general professional classes regularly in its Training Center in some of the big branches in region although most of clients engaging LPI to conduct the tailored training programs to suit their needs at their establishments.</p>
							</div>
							<div class="col-md-5" style="padding-top: 15px;padding-bottom:15px;">
								<img style="margin: auto;" src="<?= base_url() . 'img/competencies/competency7.jpg'; ?>" class="img-responsive" />
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
