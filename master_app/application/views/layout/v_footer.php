<section class="footer bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<h4 class="heading white">Contact Us</h4>
				<hr />
				<div class="row">
					<div class="col-sm-6">
						<p class="text-uppercase"><strong>Indonesia</strong></p>
						<p class="text-sm">Komplek Duta Indah Iconic E 9 - 10<br />
						Jl. M.H. Thamrin, Kebon Nanas<br />
						Panunggangan Utara, Pinang<br />
						Kota Tangerang 15143</p>
						<p class="text-sm"><i class="fa fa-envelope fa-lg"></i> &nbsp;&nbsp; <?= $this->uri->segment(1) === 'careers' ? '<a href="mailto:recruitment@linindopacific.com">recruitment@linindopacific.com</a>' : '<a href="mailto:sales.group@linindopacific.com">sales.group@linindopacific.com</a>'; ?></p>
						<p>&nbsp;</p>
					</div>
					<div class="col-sm-6">
						<p class="text-uppercase"><strong>Singapore</strong></p>
						<p class="text-sm">261 Lavender Street, #02-01<br />
						Singapore 338794</p>
						<p>&nbsp;</p>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<h4 class="heading white">Coverage</h4>
				<hr />
				<ul class="list-group coverage-title">
					<li class="list-group-item slide-active" data-slide="1"><p class="text-uppercase"><strong>Indonesia</strong></p></li>
					<li class="list-group-item" data-slide="2"><p class="text-uppercase"><strong>Worldwide</strong></p></li>
				</ul>
				<ul class="list-group coverage-master">
					<li class="list-group-item">
						<img src="<?= base_url(); ?>img/indo_coverage.png" class="img-fluid img-coverage" />
					</li>
					<li class="list-group-item">
						<img src="<?= base_url(); ?>img/world_coverage.png" class="img-fluid img-coverage" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container">
		<div class="row text-center-mobile">
			<div class="col-md-12">
				<p>&copy; <?= date('Y'); ?> Linindo Pacific International</p>
			</div>
		</div>
	</div>
</footer>
<div class="mobile-nav">
	<ul>
	</ul>
	<a href="#" class="close-link"><i class="arrow_up"></i></a>
</div>
