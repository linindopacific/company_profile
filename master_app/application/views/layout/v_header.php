<header id="intro">
	<div class="color-frame"></div>
	<div class="container">
		<div class="table">
			<div class="header-text">
				<div class="row">
					<div class="col-md-12">
						<h4 class="white">After Market Solutions for</h4>
						<h4 class="white">Heavy Mobile Equipment</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
