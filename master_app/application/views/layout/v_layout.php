<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title><?= $title; ?></title>
        <meta name="description" content="<?= $meta_description; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="robots" content="all,follow" />
        <?php foreach ($css as $key => $value) { echo '<link rel="stylesheet" href="' . $value . '" />'; } ?>
        <link rel="shortcut icon" href="<?= base_url() . 'img/favicon.png'; ?>" type="image/png" />
    </head>
    <body>
        <div class="preloader">
        	<img src="<?= base_url(); ?>img/loader.gif" alt="Preloader image">
        </div>
        <?= $content; ?>
        <?php foreach ($js as $key => $value) { echo '<script src="' . $value . '"></script>'; } ?>
        <script><?= $script; ?></script>
    </body>
 </html>
