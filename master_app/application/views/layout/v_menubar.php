<div class="menubar">
    <div class="container">
        <ul class="top-menu">
            <li class="top-menu-item">
                <a href="<?=base_url('about_us'); ?>" class="top-menu-link top-menu-dropdown">About Us</a>
                <ul class="sub-menu">
                    <li class="sub-menu-item"><a href="<?=base_url('about_us/who_we_are'); ?>" class="sub-menu-link">Who We Are</a></li>
                    <li class="sub-menu-item"><a href="<?=base_url('about_us/values'); ?>" class="sub-menu-link">Values</a></li>
                    <li class="sub-menu-item"><a href="<?=base_url('about_us/vision_mission'); ?>" class="sub-menu-link">Vision &amp; Mission</a></li>
                </ul>
            </li>
            <li class="top-menu-item">
                <a href="<?=base_url('applications'); ?>" class="top-menu-link top-menu-dropdown">Applications</a>
                <ul class="sub-menu">
                    <li class="sub-menu-item"><a href="<?=base_url('applications/mining'); ?>" class="sub-menu-link">Mining</a></li>
                    <li class="sub-menu-item"><a href="<?=base_url('applications/off_highway'); ?>" class="sub-menu-link">Off Highway</a></li>
                    <li class="sub-menu-item"><a href="<?=base_url('applications/sea_airports'); ?>" class="sub-menu-link">Sea &amp; Airports</a></li>
                </ul>
            </li>
            <li class="top-menu-item"><a href="<?=base_url('careers'); ?>" class="top-menu-link">Careers</a></li>
            <li class="top-menu-item"><a href="<?=base_url('products'); ?>" class="top-menu-link">Products</a></li>
            <li class="top-menu-item"><a href="http://portal.linindopacific.com" target="_blank" class="top-menu-link text-white bg-red"><strong>Members Only</strong></a></li>
        </ul>
    </div>
</div>
