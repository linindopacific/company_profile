<nav class="navbar">
	<div class="container">
		<div class="navbar-header">
			<span class="navbar-brand">
				<a href="<?= base_url(); ?>" class="text-logo"><img src="<?= base_url(); ?>img/logo.png" data-active-url="<?= base_url(); ?>img/logo-active.png" alt=""> &nbsp; Linindo Pacific International</a>
			</span>
		</div>
	</div>
	<?php $this->load->view('layout/v_menubar'); ?>
</nav>
