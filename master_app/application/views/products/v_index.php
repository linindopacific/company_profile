<section class="page-image">
    <img src="<?= base_url(); ?>img/image_page/products.jpg" class="img-fluid" />
</section>
<section class="content-section">
    <div class="container">
        <div class="jumbotron" style="background: transparent;">
            <table class="table-development">
                <tbody>
                    <tr>
                        <td>
                            <h2 class="heading text-center text-red">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                                    <i class="fa fa-cogs fa-stack-1x"></i>
                                </span>
                            </h2>
                        </td>
                        <td>
                            <h3 class="heading text-white">We are sorry!</h3>
                            <h5 class="heading text-white">This page is under development.</h5>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?= $this->load->view('applications/v_careers_info', NULL, TRUE); ?>
