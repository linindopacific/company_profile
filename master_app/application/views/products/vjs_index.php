<script>
(function() {
    $('.panel-product').on('click', function(e) {
        $('.product-overview.show').removeClass('show');
        var id = $(this).data('id');
        $('#'+id).addClass('show');
        $('.product-left').addClass('show-right');
        $('.product-right').addClass('show-right');
        $("html, body").animate({scrollTop: ($('#section_product').offset().top - $('nav.navbar-fixed-top .container').height() - 96)}, 1000);
    });
    $('.btn-product-close').on('click', function(e) {
        $('.product-left').removeClass('show-right');
        $('.product-right').removeClass('show-right');
    });
})();
